import React from "react";

export class Messages extends React.Component {
    render() {
        const { messages } = this.props;

        return messages && messages.map(message =>
            <p key={message.ID}><strong>{message.Sender}: {message.Body}</strong></p>)
    }
}